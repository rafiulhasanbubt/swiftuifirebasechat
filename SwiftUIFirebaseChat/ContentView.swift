//
//  ContentView.swift
//  SwiftUIFirebaseChat
//
//  Created by rafiul hasan on 7/1/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
