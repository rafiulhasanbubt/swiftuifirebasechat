//
//  FirebaseManager.swift
//  SwiftUIFirebaseChat
//
//  Created by rafiul hasan on 7/1/22.
//

import Foundation
import Firebase

class FirebaseManager: NSObject {
    
    static let shared = FirebaseManager()
    
    let auth: Auth
    let storage: Storage
    let firestore: Firestore
    var currentUser: ChatUser?
    
    override init() {
        FirebaseApp.configure()        
        self.auth = Auth.auth()
        self.storage = Storage.storage()
        self.firestore = Firestore.firestore()
        
        super.init()
    }    
}
