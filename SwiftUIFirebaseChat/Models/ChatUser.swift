//
//  ChatUser.swift
//  SwiftUIFirebaseChat
//
//  Created by rafiul hasan on 7/1/22.
//

import Foundation
import FirebaseFirestoreSwift

struct ChatUser: Codable, Identifiable {
    @DocumentID var id: String?
    let uid, email, profileImageUrl: String
}
