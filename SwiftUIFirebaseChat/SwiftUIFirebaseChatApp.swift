//
//  SwiftUIFirebaseChatApp.swift
//  SwiftUIFirebaseChat
//
//  Created by rafiul hasan on 7/1/22.
//

import SwiftUI

@main
struct SwiftUIFirebaseChatApp: App {
    var body: some Scene {
        WindowGroup {
            MainMessagesView()
        }
    }
}
